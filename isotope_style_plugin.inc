<?php

/**
 * @file
 * Provide an isotope style plugin for Views. This file is autoloaded by views.
 */

/**
 * Implementation of views_plugin_style().
 */
class isotope_style_plugin extends views_plugin_style {





  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['filters'] = array('default' => 'static');
    $options['variable-sizes'] = array('default' => 'false');
    $options['infinite-scrolling'] = array('default' => 'false');
    $options['layout-mode'] = array('default' => 'false');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $filterOptions = array(
        'static' => t('Static'),
        'taxonomy' => t('Taxonomy'),
        'dynamic' => t('Dynamic')
    );
    $variableOptions = array(
        'true' => t('Yes'),
        'false' => t('No'),
    );
    $layoutOptions = array(
        'true' => t('Yes'),
        'false' => t('No'),
    );
    $scrollingOptions = array(
        'true' => t('Yes'),
        'false' => t('No'),
    );

    $form['filters'] = array(
        '#type' => 'select',
        '#title' => t('Filtering method'),
        '#default_value' => $this->options['filters'],
        '#description' => t("Specify the filtering method"),
        '#options' => $filterOptions,
    );
    $form['variable-sizes'] = array(
        '#type' => 'select',
        '#title' => t('Variable sizes'),
        '#default_value' => $this->options['variable-sizes'],
        '#description' => t("Use variable sizes"),
        '#options' => $variableOptions,
    );
    $form['layout-mode'] = array(
        '#type' => 'select',
        '#title' => t('Layout modes'),
        '#default_value' => $this->options['layout-mode'],
        '#description' => t("show layout filters"),
        '#options' => $layoutOptions,
    );
    $form['infinite-scrolling'] = array(
        '#type' => 'select',
        '#title' => t('Infinite-scrolling'),
        '#default_value' => $this->options['infinite-scrolling'],
        '#description' => t("Use variable sizes"),
        '#options' => $scrollingOptions,
    );
  }







  /**
   * Render the display in this style.
   */
  function render() {
    $output = parent::render();

    // add js
    drupal_add_js(drupal_get_path('module', 'isotope') . '/js/jquery.isotope.min.js');
    drupal_add_js(drupal_get_path('module', 'isotope') . '/js/jquery.infinitescroll.min.js');
    drupal_add_js(drupal_get_path('module', 'isotope') . '/js/jquery.ba-bbq.min.js');

    $view_settings['row_plugin'] = get_class($this->row_plugin);


    return $output;
  }

}


























