<?php
/**
 * @file
 * Provide an isotope style plugin for Views. This file is autoloaded by views.
 */

/**
 * Implements hook_views_plugin().
 */

function isotope_filter_views_plugins() {
  return array(
    'style' => array(
      'isotope' => array(
        'title' => t('Isotope Filters'),
        'theme' => 'views_view_isotope_filter',
        'help' => t('Create filters for an isotope View'),
        'handler' => 'isotope_filter_style_plugin',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}

