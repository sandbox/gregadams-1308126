<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<section id="options" class="clearfix">
<ul id="filters" class="option-set clearfix" data-option-key="filter">
<li><a href="#filter" data-option-value="*" class="selected">All</a></li>
    <?php foreach ($rows as $id => $row): ?>
      <li><?php print $row; ?></li>
    <?php endforeach; ?>
</ul>
    <ul id="sort-by" class="option-set clearfix" data-option-key="sortBy">
      <li><a href="#sortBy=original-order" data-option-value="original-order" class="selected" data>original-order</a></li>
      <li><a href="#sortBy=category" data-option-value="category">category</a></li>
      <li><a href="#sortBy=random" data-option-value="random">random</a></li>
    </ul>
    <ul id="sort-direction" class="option-set clearfix" data-option-key="sortAscending">
      <li><a href="#sortAscending=true" data-option-value="true" class="selected">sort ascending</a></li>
      <li><a href="#sortAscending=false" data-option-value="false">sort descending</a></li>
    </ul>
</section>