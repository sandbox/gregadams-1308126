<?php
/**
 * @file
 * Provide an isotope style plugin for Views. This file is autoloaded by views.
 */

/**
 * Implements hook_views_plugin().
 */
function isotope_views_plugins() {
  return array(
    'style' => array(
      'isotope' => array(
        'title' => t('Isotope View'),
        'theme' => 'views_view_isotope',
        'help' => t('Uses the jQuery Isotope plugin by metafizzy to reorder or filter your content.'),
        'handler' => 'isotope_style_plugin',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
      ),
      'isotope_filter' => array(
        'title' => t('Isotope Filters'),
        'theme' => 'views_view_isotope_filter',
        'help' => t('Create filters for an Isotope View'),
        'handler' => 'isotope_filter_style_plugin',
        'uses row plugin' => TRUE,
        'uses row class' => TRUE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'uses grouping' => FALSE,
        'type' => 'normal',
      ),
    ),
  );

}

