<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>

  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>

  <div id="container" class="clearfix">
	<?php foreach ($rows as $id => $row): ?>
		<div class="<?php print $classes_array[$id]; ?> element" data-category="<?php 
					$txt=$classes_array[$id]; 
$out=preg_split('/\s+/',trim($txt)); 
echo $out[count($out)-1];  
			?>">
<?php print $row;?></div>
    <?php endforeach; ?>
  </div>
