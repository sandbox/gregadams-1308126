#################################################
JQUERY ISOTOPE BY METAFIZZY FOR DRUPAL 7
#################################################
THIS IS A DEV VERSION.
#################################################
DO NOT USE THIS MODULE ON PRODUCTION SITES
#################################################

MODULE BY GREGOR ADAMS - PIXELASS - 17. Oktober 2011 23:03:33 MESZ

#################################################
   #################################################
   #################################################
     INSTALLATION
#################################################

1. Install CTOOLS
1. Install VIEWS
2. Install the module "Isotope"

#################################################
   #################################################
   #################################################
     SETUP CONTENT TYPE
#################################################

The module provides a custom Content type that can be used to add isotope nodes.
Of course you can use any other Content type too.

1. go to your "Isotope" Content type
2. Set the term reference field to your desired vocabulary

#################################################
   #################################################
   #################################################
     ADDING TERMS
#################################################

The module provides a custom category that can be used to filter or sort your items.
Of course you can use any other vocabulary too.

#################################################
   #################################################
   #################################################
     ADDING FILTERS
#################################################

1. create a view with "Isotope FIlters" Views style.
   it needs to be set to "taxonomy terms" (not content).
2. create a block for this view.
3. rewrite the output of the field "term (name)"
          <a href="#filter" data-option-value=".[name]">[name]</a>

Now you can put the block in any region you want or in the header of your isotope view.


#################################################
   #################################################
   #################################################
     THE ISOTOPE VIEW
#################################################

1. create a view (content) with "Isotpe View" Views style.
2. set the display to fields
3. add a field for your taxonomy term (you can exclude this from display if you want)
4. go to the "Isotope View" Display settings and add the replacement pattern as a row class 
   (for "Tags" it would be [field_tags])
   (for "Isotope Filters" it would be [isotope_term])
   you can have multiple replacement patterns separated by a "space"
   the last added class will be the data-category (for sort by category)
5. You can add you filter block in the header of that view (View area)
6. add a custom script to the footer with php enabled

(this needs to be added manually to the views footer in a textarea with php filter enabled)
something like this should do (see my demos or read the documentation on http://isotope.metafizzy.com )

------  currently all "$" need to be changed to "jQuery" for it to work. ------

<script>
jQuery(function() {
    var jQuerycontainer = jQuery('#container');
    jQuerycontainer.isotope({
        itemSelector: '.element',
        masonry: {
            columnWidth: 230
        },
        masonryHorizontal: {
            rowHeight: 230
        }
    });
    var jQueryoptionSets = jQuery('#options .option-set'),
        jQueryoptionLinks = jQueryoptionSets.find('a');
    jQueryoptionLinks.click(function() {
        var jQuerythis = jQuery(this);
        // don't proceed if already selected
        if (jQuerythis.hasClass('selected')) {
            return false;
        }
        var jQueryoptionSet = jQuerythis.parents('.option-set');
        jQueryoptionSet.find('.selected').removeClass('selected');
        jQuerythis.addClass('selected');
        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = jQueryoptionSet.attr('data-option-key'),
            value = jQuerythis.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[key] = value;
        if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
            // changes in layout modes need extra logic
            changeLayoutMode(jQuerythis, options);
        } else {
            // otherwise, apply new options
            jQuerycontainer.isotope(options);
        }
        return false;
    });
});
</script>

#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
#################################################


It's not fully dynamic yet but will be soon, I promise... ;)

I tested it on my theme and the Bartik theme. It should be compatible to other themes too though.

Just take a look at my examples http://isotope.pixelass.com
it explains how to use static filters and shows a few footer-script examples

If you need any help feel free to ask on drupal.org or via email  info@pixelass.com .



#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
